var index = 0;

$( document ).ready(function() {
    $('.login_form').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: "json",

            success: function (response) {
                console.log(response);
                if(response.success==true){
                    window.location.replace(response.redirect);
                } else {
                    $("#username").addClass("false-red");
                    $("#password").addClass("false-red");
                }
            },

            error: function (response) {
                console.log(response);
            }
        });
    });

    var play = $(".hero-image-scroll-down").hover(over, out);

    function over(){
        TweenMax.to(play, 0.3, {rotation:-180});
    }

    function out(){
        TweenMax.to(play, 0.3, {rotation:0});
    }


    if (window.matchMedia("(max-width: 940px)").matches) { //RESIZE WINDOW
        TweenMax.from(".image-header-index", 40, {backgroundPosition: "0% 100%", repeat:1, yoyo:true, ease:Linear.easeNone});
    } else {
        TweenMax.from(".image-header-index", 0, {backgroundPosition: "auto", repeat:0, yoyo:false});
    }


//BOUTON MOTO SLIDE 1 BAS
    /*
    $(".bouton-after-bg1-right").click(function(){
        $(".part1-bg-img1").animate({
            'background-position-x': '100%'
        },{queue: false, duration: 'slow'});
    });

    $(".bouton-after-bg1-left").click(function(){
        $(".part1-bg-img1").animate({
            'background-position-x': '0%'
        },{queue: false, duration: 'slow'});
    });
*/

//BOUTON MOTO SLIDE 2 HAUT
 /*   $(".bouton-after-bg2-right").click(function(){
        $(".part1-bg-img2").animate({
            'background-position-x': '100%'
        },{queue: false, duration: 'slow'});
    });

    $(".bouton-after-bg2-left").click(function(){
        $(".part1-bg-img2").animate({
            'background-position-x': '0%'
        },{queue: false, duration: 'slow'});
    });
*/



    //Animation MENU
    // init controller
    var controller = new ScrollMagic.Controller();
    // build scene
    var scene = new ScrollMagic.Scene({
        triggerElement: "#trigger1",
        triggerHook: "onLeave",
        offset: 50,
        /*duration:100*/
    })
        .setTween("#menu-animation", 0.5, {backgroundColor: "rgba(30,30,30,1)", position:"fixed"})
        //.addIndicators({name: "1 (duration: full)"}) // add indicators (requires plugin)
        .addTo(controller);


    //Animation POP-UP MAP
    tweenPopUp = new TimelineMax()
        .add([
            TweenMax.fromTo(".pop-up", 0.6, {opacity:'0',left: '-300px', visibility: 'hidden'}, {opacity:'0.95',left: '0px', visibility: 'visible', ease: Power3.easeOut})
        ]);

    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: "#map", triggerHook: "onLeave", offset:-250, reverse:false})
        .setTween(tweenPopUp)
        //.addIndicators({name:"Pop-up"})
        .addTo(controller);

    // DISPLAY NONE POP-UP
    $(".close-pop-up").click(function(){
        $(".pop-up").css("display" , "none");
    });


    if (window.matchMedia("(min-width: 940px)").matches) { //RESIZE WINDOW
        //Animation REVELATION BLOC TEXTE INDEX
        tween2 = new TimelineMax()
            .add([
                TweenMax.fromTo(".container-section-1", 1.5, {opacity: 0,yPercent: '20%'}, {opacity:1,yPercent: '0%', ease: Power3.easeOut})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#row-bloc1-index", offset:100, reverse: false})
            .setTween(tween2)
            //.addIndicators({name:"bloc 2 index"})
            .addTo(controller);

        //Animation SCROLL MOTO INDEX
        tweenScrollMoto = new TimelineMax()
            .add([
                TweenMax.fromTo(".part1-bg-img1", 3, {backgroundPosition:"0 0"}, {backgroundPosition:"100% 0", ease:Linear.easeNone})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: ".part1-bg-img1", triggerHook: "onLeave", offset:0, duration:1000})
            .setTween(tweenScrollMoto)
            //.addIndicators({name:"tween Para MOTO"})
            .addTo(controller);


        //Animation CATALOGUE SCROLL PARALLAXE
        tweenParaCata = new TimelineMax()
            .add([
                TweenMax.fromTo(".text-header-catalogue", 3, {opacity: 1,yPercent: '0%'}, {opacity:0,yPercent: '120%'}),
                TweenMax.fromTo(".text-header-abonnement", 3, {opacity: 1,yPercent: '0%'}, {opacity:0,yPercent: '120%'}),
                TweenMax.fromTo(".text-header-localisation", 3, {opacity: 1,yPercent: '0%'}, {opacity:0,yPercent: '120%'})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#trigger1", triggerHook: "onLeave", offset:30, duration:600})
            .setTween(tweenParaCata)
            //.addIndicators({name:"tween Para Index"})
            .addTo(controller);




        //Animation BLOC BIKER ZONE INDEX
        tween2 = new TimelineMax()
            .add([
                TweenMax.fromTo(".container-section-2", 1.2, {width: '0%'}, {width: '100%', ease: Power3.easeOut}),
                TweenMax.fromTo(".border-vertical", 1, {opacity:'0', height: '0', top: '0'}, {opacity:'1', height: '65px', top: '65px', ease: Power3.easeOut, delay:1.2})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#section-index-2", offset:-260, reverse:false})
            .setTween(tween2)
            //.addIndicators({name:"bloc biker-zone"})
            .addTo(controller);


        //Animation CATALOGUE LIGNE VERTICAL
        tween3 = new TimelineMax()
            .add([
                TweenMax.fromTo(".border-vertical-catalogue", 1, {opacity:'0', height: '0', top: '0'}, {opacity:'1', height: '65px', top: '50px', ease: Power3.easeOut, delay:1.2})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#section-catalogue-2", offset:-260, reverse:false})
            .setTween(tween3)
            //.addIndicators({name:"bloc biker-zone"})
            .addTo(controller);


        //Animation BLOC CATALOGUE + LIGNE VERTICAL
        tween4 = new TimelineMax()
            .add([
                TweenMax.fromTo(".container-abonnement-section-2", 1.2, {width: '0%'}, {width: '100%', ease: Power3.easeOut}),
                TweenMax.fromTo(".border-vertical-abonnement", 1, {opacity:'0', height: '0', top: '0'}, {opacity:'1', height: '65px', top: '65px', ease: Power3.easeOut, delay:1.2})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#section-abonnement-2", offset:-260, reverse:false})
            .setTween(tween4)
            //.addIndicators({name:"bloc biker-zone"})
            .addTo(controller);
    } else {
        //Animation CATALOGUE SCROLL PARALLAXE
        tweenParaCata = new TimelineMax()
            .add([
                TweenMax.fromTo(".text-header-catalogue", 3, {opacity: 1,yPercent: '0%'}, {opacity:0,yPercent: '120%'}),
                TweenMax.fromTo(".text-header-abonnement", 3, {opacity: 1,yPercent: '0%'}, {opacity:0,yPercent: '120%'}),
                TweenMax.fromTo(".text-header-localisation", 3, {opacity: 1,yPercent: '0%'}, {opacity:0,yPercent: '120%'})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#trigger1", triggerHook: "onLeave", offset:30, duration:600})
            .setTween(tweenParaCata)
            //.addIndicators({name:"tween Para Index"})
            .addTo(controller);


        //Animation BLOC BIKER ZONE INDEX
        tween2 = new TimelineMax()
            .add([
                /*TweenMax.fromTo(".container-section-2", 1.2, {width: '0%'}, {width: '100%', ease: Power3.easeOut}),*/
                TweenMax.fromTo(".border-vertical", 1, {opacity:'0', height: '0', top: '0'}, {opacity:'1', height: '45px', top: '65px', ease: Power3.easeOut, delay:1.2})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#section-index-2", offset:-260, reverse:false})
            .setTween(tween2)
            //.addIndicators({name:"bloc biker-zone"})
            .addTo(controller);


        //Animation CATALOGUE LIGNE VERTICAL
        tween3 = new TimelineMax()
            .add([
                TweenMax.fromTo(".border-vertical-catalogue", 1, {opacity:'0', height: '0', top: '0'}, {opacity:'1', height: '45px', top: '50px', ease: Power3.easeOut, delay:1.2})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#section-catalogue-2", offset:-260, reverse:false})
            .setTween(tween3)
            //.addIndicators({name:"bloc biker-zone"})
            .addTo(controller);



        //Animation BLOC CATALOGUE + LIGNE VERTICAL
        tween4 = new TimelineMax()
            .add([
                /*TweenMax.fromTo(".container-abonnement-section-2", 1.2, {width: '0%'}, {width: '100%', ease: Power3.easeOut}),*/
                TweenMax.fromTo(".border-vertical-abonnement", 1, {opacity:'0', height: '0', top: '0'}, {opacity:'1', height: '45px', top: '45px', ease: Power3.easeOut, delay:1.2})
            ]);

        // build scene
        var scene = new ScrollMagic.Scene({triggerElement: "#section-abonnement-2", offset:-260, reverse:false})
            .setTween(tween4)
            //.addIndicators({name:"bloc biker-zone"})
            .addTo(controller);
    }



    <!-- Script pour mon Super Bouton HOVER -->

    $(".button-outline").hover(
        function () {
            $(".button-outline-copy").addClass("button_hover");
            $(".button-outline-bottom").addClass("button_hover_bas");
        },
        function () {
            $(".button-outline-copy").removeClass("button_hover");
            $(".button-outline-bottom").removeClass("button_hover_bas");
        }
    );

    <!-- Script pour hover ORDER COMMANDER BOUTON-->
    $(".cart-cataloque").hover(function() {
        TweenMax.to(".hover-order-menu", 1, {display: "block", opacity:"1", ease: Power3.easeOut});//maxHeight: "500px"
        }, function(){
        TweenMax.to(".hover-order-menu", 1, {display: "none", opacity:"0", ease: Power3.easeOut});
        }
    );

    $(".button-order-black-menu").hover(function() {
            TweenMax.to(".icon-order-menu", 0.5, {opacity:"1", paddingLeft: "20px", ease: Power3.easeOut});
        }, function(){
            TweenMax.to(".icon-order-menu", 0.5, {opacity:"0", paddingLeft: "0px", ease: Power3.easeOut});
        }
    );

    /* ANIMATION CLICK ARTICLE */
    $(".button-articles-anim").click(function(){
        TweenMax.fromTo($(this).find(".fa-shopping-cart"), 0.4, {left :"-20px"},{left :"10px", delay:3});
        TweenMax.fromTo($(this).find(".svg-articles-button"), 1, {strokeDashoffset: "19.80"},{strokeDashoffset: "0"});
        TweenMax.fromTo($(this).find(".svg-articles-button"), 0.4, {left :"5px"},{left :"40px", delay:3});
    });


    /* --- ANIMATION BOX SHARE --- */
    var colors = ["$color-brown-h2"];

//initially colorize each box and position in a row
    TweenMax.set(".box", {
        backgroundColor:function(i) {
            return colors[i % colors.length];
        },
        x:function(i) {
            return i * 100;
        }
    });


    TweenMax.to(".box", 15, {
        ease: Linear.easeNone,
        x: "+=400", //move each box 500px to right
        modifiers: {
            x: function(x) {
                return x % 400; //force x value to be between 0 and 500 using modulus
            }
        },
        repeat: -1
    });


    //BX SLIDER POUR LES ARTICLES
    var i = 0;
    $('.bxslider').each(function(){
        //console.log($('.bxslider:eq('+i+')'));
        $('.bxslider:eq('+i+')').bxSlider({
            controls: false,
            pagerCustom: '.bx-pager-'+i,
            onSliderLoad: function() {
                $(this).find("li:not([class='bx-clone'])").eq(0).addClass('active-caption');
                var url = $(this).find("li:not([class='bx-clone'])").eq(0).attr('data-path');
                $(this).closest('.card-box').find('.button-header-catalogue-articles').attr('data-action', url);
            },
            onSlideBefore: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                $('.active-caption').removeClass('active-caption');
                $(this).find("li:not([class='bx-clone'])").eq(currentSlideHtmlObject).addClass('active-caption');
                var url = $(this).find("li:not([class='bx-clone'])").eq(currentSlideHtmlObject).attr('data-path');
                $(this).closest('.card-box').find('.button-header-catalogue-articles').attr('data-action', url);
            }

        });
        i++;
    });

    $('.bx-pager-btn').on('click', function(e){
        e.preventDefault();
        console.log($(this).attr('data-slide-index'));
        //slider.goToSlide(2);

    });

    // SCRIPT START TABS
    $("#tabs").tabs({show: 'fade', hide: 'fade'});
    $("#tabs2").tabs({show: 'fade', hide: 'fade'});

    // TEXT ROTATOR JQUERY
    $('#rotate').rotaterator({fadeSpeed:800, pauseSpeed:4000});
    // TEXT ROTATOR JQUERY
});

// TEXT ROTATOR JQUERY
$.fn.extend({
    rotaterator: function(options) {

        var defaults = {
            fadeSpeed: 800,
            pauseSpeed: 4000,
            child:null
        };

        var options = $.extend(defaults, options);

        return this.each(function() {
            var o =options;
            var obj = $(this);
            var items = $(obj.children(), obj);
            items.each(function() {$(this).hide();})
            if(!o.child){var next = $(obj).children(':first');
            }else{var next = o.child;
            }
            $(next).fadeIn(o.fadeSpeed, function() {
                $(next).delay(o.pauseSpeed).fadeOut(o.fadeSpeed, function() {
                    var next = $(this).next();
                    if (next.length == 0){
                        next = $(obj).children(':first');
                    }
                    $(obj).rotaterator({child : next, fadeSpeed : o.fadeSpeed, pauseSpeed : o.pauseSpeed});
                })
            });
        });
    }
});
// TEXT ROTATOR JQUERY



$(document).on('click', '.ajaxSend', function (e){
    e.preventDefault();
    //console.log($(this));
    sendProductAjax($(this).attr('data-action'));
});

// After resize events RESIZE WINDOW
/*
var id;
var startWidth = window.innerWidth;*/ //get the original screen width
/*
$(window).resize(function() {
    clearTimeout(id);
    id = setTimeout(doneResizing, 500);
});
function doneResizing(){
    if ($(window).width() < 940) {
        this.location.reload(false);
    } else {
        if (startWidth < 940){
            this.location.reload(false);
        }
    }
}*/

$(window).load(function() {
    var heightVimeo = $('#height-vimeo').outerHeight();
    $('.fm__bar').css({ 'height': 'calc((100% - ' + heightVimeo+ 'px)/2)'});

    if($('.fm__bar').height() === 50){
        $('.plyr__controls').css({"padding-top":"9px"});
        $('.close').css({"bottom":"16px"});
    }
    else {
        $('.plyr__controls').css({"padding-top":""});
        $('.close').css({"bottom":""});
    }
});

$(window).resize(function() {
    var heightVimeo = $('#height-vimeo').outerHeight();
    $('.fm__bar').css({ 'height': 'calc((100% - ' + heightVimeo+ 'px)/2)'});
    //console.log(heightVimeo);
    if($('.fm__bar').height() === 50){
        $('.plyr__controls').css({"padding-top":"9px"});
        $('.close').css({"bottom":"16px"});
    }
    else {
        $('.plyr__controls').css({"padding-top":""});
        $('.close').css({"bottom":""});
    }
});


//<!-- --- Script pour ma VIMEO --- -->

$(".movie").click(function(){
    $('.fm').css({visibility:"visible"});
    TweenMax.fromTo(".fm__layer", 0.5, {height:"0%"}, {height:"100%", delay:0.4});
    $('.fm__player').css({opacity:"1"});



    var $fm__bar = $('.fm__bar-haut');
    $fm__bar.removeClass('fm__bar--top');
    $fm__bar.addClass('fm__bar--top');
    TweenMax.fromTo(".fm__bar--top", 2, {opacity: "1", yPercent :-100}, {opacity: "1", yPercent :0, delay: 0.8,  ease: Power3.easeOut});



    var $fm__bar = $('.fm__bar-bas');
    $fm__bar.removeClass('fm__bar--bottom');
    $fm__bar.addClass('fm__bar--bottom');
    TweenMax.fromTo(".fm__bar--bottom", 2, {opacity: "1", yPercent :100}, {opacity: "1", yPercent :0, delay: 0.8,  ease: Power3.easeOut});


/* Player video declenchement */

    var $fm__bar = $('.plyr');
    TweenMax.fromTo(".plyr", 0.5, {opacity: "0"}, {opacity: "1", delay: 0.6,  ease: Power1.easeOut});

/* container video opacity */

    var $fm__bar = $('.fm__player__video');
    TweenMax.fromTo(".fm__player__video", 0.5, {opacity: "0"}, {opacity: "1", delay: 1,  ease: Power1.easeOut});
});


// VIDEO CONTROLS ---------------------------------------------


// PLAY VIDEO AUTOPLAY
$('.movie').on('click', function(ev) {
    $("#video")[0].src += "&autoplay=1";
    ev.preventDefault();
});


// STOP VIDEO

$('.close').click(function() {
    $("#video").vimeo("unload");
});

// PLAY VIDEO
$('#play').click(function() {
    $("#video").vimeo("play");
    play();
});

// PAUSE VIDEO
$('#pause').click(function() {
    $("#video").vimeo("pause");
    pause();
});



$("#video").vimeo("getDuration", function(data){
    $('.time-total').text(formatSecondsAsTime(data));
})

$("#video").on("playProgress", function(event, data){
    $("#video").vimeo("getCurrentTime", function(data){
        $('.plyr__time--current').text(formatSecondsAsTime(data));
    })
    $('.ligne-progress-bar').css('width', getPercent(data)+'%');
});

$("#video").on("finish", function(){
    console.log( "Video is finish" );
});



// SWITCH play pause
function play() {
    $("#play").css("display", "none");
    $("#pause").css("display", "inline-block");
}

function pause() {
    $("#play").css("display", "inline-block");
    $("#pause").css("display", "none");
}


function formatSecondsAsTime(secs) {

    var hr  = Math.floor(secs / 3600);
    var min = Math.floor((secs - (hr * 3600))/60);
    var sec = Math.floor(secs - (hr * 3600) -  (min * 60));

    if (hr < 10)   { hr    = "0" + hr; }
    if (min < 10) { min = "0" + min; }
    if (sec < 10)  { sec  = "0" + sec; }

    if (hr != '00'){
        hr   = "00";
        return hr + ':' + min + ':' + sec;
    } else {
        return min + ':' + sec;
    }

}

function getPercent(currentTime){
    return (currentTime.percent)*100;
}

// Start animation in the opposite direction
$( ".close" ).click(function() {
    $( ".fm" ).css({visibility:"hidden"});
    $('.fm__layer').css({height:"0%"});
    $('.fm__player').css({opacity:"0"});
});

// Animation TweenMax ScrollMagic



// Script pour mon Super Bouton HOVER 2
$(".button-outline2").hover(
    function () {
        $(".button-outline-copy").addClass("button_hover2");
        $(".button-outline-bottom").addClass("button_hover_bas2");
    },
    function () {
        $(".button-outline-copy").removeClass("button_hover2");
        $(".button-outline-bottom").removeClass("button_hover_bas2");
    }
);

// Script pour mon Super Bouton HOVER 2
$(".button-outline3").hover(
    function () {
        $(".button-outline-copy").addClass("button_hover3");
        $(".button-outline-bottom").addClass("button_hover_bas3");
    },
    function () {
        $(".button-outline-copy").removeClass("button_hover3");
        $(".button-outline-bottom").removeClass("button_hover_bas3");
    }
);

/*$(function () {
    $('.ui-tabs-nav a').on('click', function(){
        TweenMax.fromTo(".ui-tabs-panel", 1, {opacity: 0}, {opacity: 1});
    });*/



// ANIMATION SECTION 4 INDEX SCROLL EFFECT

$(function () { // wait for document ready
                // init controller
    var controller = new ScrollMagic.Controller({loglevel: 3});

    // build tween
    var tween = TweenMax.to(".part2-left", 2, {backgroundColor: "#814453", delay:1});//5e323c




    // build scene
    var scene = new ScrollMagic.Scene({triggerElement: ".trigger-center", duration: "100%", loglevel: 3})
        .setTween(tween)
        .setPin(".part1-bg-img1")
        //.addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    // checkbox actions
    $("form.loglevel input[type=checkbox]").on("change", function (e) {
        var
            target = $(this).attr("id") == "logcontroller" ? controller : scene,
            level = $(this).prop("checked") ? 3 : 0;

        target.loglevel(level);
    });
});


var controller = new ScrollMagic.Controller();
tween4 = new TimelineMax()
// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#map", offset:320,reverse:false}) //duration: 550 duration: $(window).height()
    .on('start', function () {
        drop();
    })
    .setTween(tween4)
    //.addIndicators({name:"Map Animation"})
    .addTo(controller);

$('.image-article-slider').hover(
    function() {
        TweenMax.to(this, .5, {css: {scaleX: 1.1, scaleY: 1.1},ease: Circ.easeOut});
    }, function() {
        TweenMax.to(this, .5, {css: {scaleX: 1, scaleY: 1},ease: Circ.easeOut});
    });

// MODAL SHARE
var modalshare = document.getElementById('share');

// Get the button that opens the modal
var btnshare = document.getElementById("bouton-share");

// Get the <span> element that closes the modal
var spanshare = document.getElementsByClassName("close-modal-share")[0];

// When the user clicks the button, open the modal
btnshare.onclick = function() {
    modalshare.style.display = "block";
    TweenMax.fromTo(".modal", 0.3, {opacity: 0}, {opacity: 1});
    TweenMax.fromTo(".ligne-gauche-share", 1, {width: 0}, {width: 90, delay: 1, ease: Power3.easeOut});
    TweenMax.fromTo(".ligne-droite-share", 1, {width: 0}, {width: 90, delay: 1, ease: Power3.easeOut});
};

// When the user clicks on <span> (x), close the modal
spanshare.onclick = function() {
    modalshare.style.display = "none";
};

// MODAL CONNEXION
var modal = document.getElementById('connexion');

// Get the button that opens the modal
var btn = document.getElementById("bouton-connexion");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close-modal")[0];

// When the user clicks the button, open the modal
if(btn !== null && btn!==undefined){
    btn.onclick = function() {
        modal.style.display = "block";
        TweenMax.fromTo(".modal", 0.3, {opacity: 0}, {opacity: 1});
    }
};


// When the user clicks on <span> (x), close the modal
if(span!==null && span!==undefined){
    span.onclick = function() {
        modal.style.display = "none";
    }
};



// MODAL Inscription
var modalinscription = document.getElementById('inscription');

// Get the button that opens the modal
/*var btninscription = document.getElementById("bouton-inscription");*/

// Get the <span> element that closes the modal
var spaninscription = document.getElementsByClassName("close-modal-inscription")[0];

$('.bouton-inscription').on('click',function(e){
    e.preventDefault();
    modalinscription.style.display = "block";
    TweenMax.fromTo(".modal", 0.3, {opacity: 0}, {opacity: 1});
});

// When the user clicks on <span> (x), close the modal
if(spaninscription!==null&& spaninscription!==undefined){
    spaninscription.onclick = function() {
        modalinscription.style.display = "none";
    }
};


// MODAL POP-UP ALERT
var modalpopup = document.getElementById('alert-pop-up');

// Get the <span> element that closes the modal
var spanpopup = document.getElementsByClassName("close-modal-pop-up")[0];

$('.not-connected').on('click',function(e){
    e.preventDefault();
    modalpopup.style.display = "block";
    TweenMax.fromTo(".modal", 0.3, {opacity: 0}, {opacity: 1});
});

// When the user clicks on <span> (OK), close the modal
if(spaninscription!==null&& spanpopup!==undefined){
    spanpopup.onclick = function() {
        modalpopup.style.display = "none";
    }
};



// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    switch(event.target){
        case modalshare:
            modalshare.style.display = "none";
            break;
        case modalinscription:
            modalinscription.style.display = "none";
            break;
        case modal:
            modal.style.display = "none";
            break;
        case modalpopup:
            modalpopup.style.display = "none";
            break;
        default:
            break;
    }
};

function sendProductAjax(url){
    $.ajax({
        url: url,
        type: "POST",
        success: function (response) {
            $('.cart-cataloque').html(response.data_cart);
            $('.container-order-section-2').html(response.data_order);
        }
    });
}





