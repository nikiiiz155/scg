<?php
namespace SCG\ShopBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use SCG\ShopBundle\Entity\Category;
use SCG\ShopBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name',TextType::class)
            ->add('category',EntityType::class,array(
                'class' => Category::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false
            ))
            ->add('subCategory',TextType::class)
            ->add('price',NumberType::class)
            ->add('descriptionTitle',TextType::class)
            ->add('color',TextType::class)
            ->add('description',TextareaType::class)
            ->add('media',TextType::class,array(
                'required' => false
            ))
            ->add('parentProduct',EntityType::class,array(
                'class' => Product::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.parentProduct is NULL');
                },
                'choice_label' => 'subCategory',
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'No Parent Product',
                'empty_data'  => null,
                'required' => false
            ))
            ->add('isSubscription',CheckboxType::class, array(
                'required' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => Product::class
        ));
    }

    public function getBlockPrefix(){
        return 'scgshop_bundle_product_type';
    }
}
