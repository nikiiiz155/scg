<?php
namespace SCG\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SCG\ShopBundle\Entity\Cart;
use SCG\ShopBundle\Entity\Product;

/**
 * @ORM\Entity
 * @ORM\Table(name="cartProduct")
 */
class CartProduct{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="SCG\ShopBundle\Entity\Product", inversedBy="cartProduct")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="SCG\ShopBundle\Entity\Cart", inversedBy="cartProduct")
     */
    protected $cart;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return CartProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return CartProduct
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set cart
     *
     * @param Cart $cart
     *
     * @return CartProduct
     */
    public function setCart(Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }
}
