<?php
namespace SCG\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use SCG\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use SCG\ShopBundle\Entity\CartProduct;

/**
 * @ORM\Entity
 * @ORM\Table(name="cart")
 */
class Cart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @OneToOne(targetEntity="SCG\UserBundle\Entity\User")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="SCG\ShopBundle\Entity\CartProduct", mappedBy="cart")
     */
    protected $cartProduct;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Cart
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartProduct = new ArrayCollection();
    }

    /**
     * Add cartProduct
     *
     * @param CartProduct $cartProduct
     *
     * @return Cart
     */
    public function addCartProduct(CartProduct $cartProduct)
    {
        $this->cartProduct[] = $cartProduct;

        return $this;
    }

    /**
     * Remove cartProduct
     *
     * @param CartProduct $cartProduct
     */
    public function removeCartProduct(CartProduct $cartProduct)
    {
        $this->cartProduct->removeElement($cartProduct);
    }

    /**
     * Get cartProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartProduct()
    {
        return $this->cartProduct;
    }
}
