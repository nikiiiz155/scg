<?php
namespace SCG\ShopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use SCG\ShopBundle\Entity\CartProduct;
use SCG\ShopBundle\Entity\Category;

/**
 * @ORM\Entity(repositoryClass="SCG\ShopBundle\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $subCategory;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $price;

    /**
     * @ORM\Column(type="text")
     */
    protected $descriptionTitle;

    /**
     * @ORM\Column(type="text")
     */
    protected $color;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isSubscription;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $media;

    /**
     * @ManyToOne(targetEntity="Category", inversedBy="products")
     */
    protected $category;

    /**
     * One Product has Many Sub-Products.
     * @OneToMany(targetEntity="Product", mappedBy="parentProduct")
     */
    protected $subProducts;

    /**
     * Many Sub-Products have One parent product.
     * @ManyToOne(targetEntity="Product", inversedBy="subProducts")
     */
    protected $parentProduct;

    /**
     * @ORM\OneToMany(targetEntity="SCG\ShopBundle\Entity\CartProduct", mappedBy="product")
     */
    protected $cartProduct;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subProducts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cartProduct = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set subCategory
     *
     * @param string $subCategory
     *
     * @return Product
     */
    public function setSubCategory($subCategory)
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    /**
     * Get subCategory
     *
     * @return string
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set descriptionTitle
     *
     * @param string $descriptionTitle
     *
     * @return Product
     */
    public function setDescriptionTitle($descriptionTitle)
    {
        $this->descriptionTitle = $descriptionTitle;

        return $this;
    }

    /**
     * Get descriptionTitle
     *
     * @return string
     */
    public function getDescriptionTitle()
    {
        return $this->descriptionTitle;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return Product
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Product
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \SCG\ShopBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add subProduct
     *
     * @param Product $subProduct
     *
     * @return Product
     */
    public function addSubProduct(Product $subProduct)
    {
        $this->subProducts[] = $subProduct;

        return $this;
    }

    /**
     * Remove subProduct
     *
     * @param Product $subProduct
     */
    public function removeSubProduct(Product $subProduct)
    {
        $this->subProducts->removeElement($subProduct);
    }

    /**
     * Get subProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubProducts()
    {
        return $this->subProducts;
    }

    /**
     * Set parentProduct
     *
     * @param Product $parentProduct
     *
     * @return Product
     */
    public function setParentProduct(Product $parentProduct = null)
    {
        $this->parentProduct = $parentProduct;

        return $this;
    }

    /**
     * Get parentProduct
     *
     * @return \SCG\ShopBundle\Entity\Product
     */
    public function getParentProduct()
    {
        return $this->parentProduct;
    }

    /**
     * Add cartProduct
     *
     * @param CartProduct $cartProduct
     *
     * @return Product
     */
    public function addCartProduct(CartProduct $cartProduct)
    {
        $this->cartProduct[] = $cartProduct;

        return $this;
    }

    /**
     * Remove cartProduct
     *
     * @param CartProduct $cartProduct
     */
    public function removeCartProduct(CartProduct $cartProduct)
    {
        $this->cartProduct->removeElement($cartProduct);
    }

    /**
     * Get cartProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartProduct()
    {
        return $this->cartProduct;
    }

    /**
     * Set isSubscription
     *
     * @param boolean $isSubscription
     *
     * @return Product
     */
    public function setIsSubscription($isSubscription)
    {
        $this->isSubscription = $isSubscription;
    
        return $this;
    }

    /**
     * Get isSubscription
     *
     * @return boolean
     */
    public function getIsSubscription()
    {
        return $this->isSubscription;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Product
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
