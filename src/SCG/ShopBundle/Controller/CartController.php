<?php
namespace SCG\ShopBundle\Controller;

use SCG\ShopBundle\Entity\Cart;
use SCG\ShopBundle\Entity\CartProduct;
use SCG\ShopBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class CartController extends Controller
{
    public function viewAction(){

        $em = $this->getDoctrine()->getManager();

        $cartProducts = $this->getDoctrine()->getRepository(CartProduct::class)->findAll();
        $productArray = [];
        foreach ($cartProducts as $product ){
            if($product->getCart() == $this->getUser()->getCart()){
                array_push($productArray, $product);
            }
        }
        $parentProducts = $em->getRepository(Product::class)->findAllParentProducts();
        $subProducts = $em->getRepository(Product::class)->findAllSubProducts();
        $cart = $em->getRepository(Cart::class)->findOneByUser($this->getUser());
        return $this->render('SCGShopBundle:Cart:cart.html.twig', array(
            'parentProducts' => $parentProducts,
            'subProducts' => $subProducts,
            'cart' => $cart,
            'cartProducts' => $productArray,
        ));
    }

    public function addProductToCartAction($productId){
        $success = false;

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $cart = null;

        $product = $em->getRepository(Product::class)->find($productId);
        if (!$user->getCart()){

            $cart = new Cart();
            $user->setCart($cart);
            $cart->setUser($user);

            $cartProduct = new CartProduct();
            $cartProduct->setCart($cart);
            $cartProduct->setProduct($product);
            $cartProduct->setQuantity(1);

            $em->persist($user);
            $em->persist($cart);
            $em->flush();

            $message = 'Produit ajouté au panier.';
            $success = true;

        } else {
            $cart = $user->getCart();

            $cartProduct = $em->getRepository(CartProduct::class)->findOneBy(array(
                'cart' => $cart,
                'product' => $product
            ));

            if ($cartProduct){
                $cartProduct->setQuantity($cartProduct->getQuantity()+ 1);
            }else{
                $cartProduct = new CartProduct();
                $cartProduct->setCart($cart);
                $cartProduct->setProduct($product);
                $cartProduct->setQuantity(1);
            }

            $message = 'Produit ajouté au panier.';
            $success = true;
        }

        $em->persist($cartProduct);
        $em->flush();

        /*return $this->redirectToRoute('scg_shop_homepage');*/

        $cartProducts = $this->getDoctrine()->getRepository(CartProduct::class)->findBy(array(
            'cart' => $cart
        ));
        $data = $this->renderView('SCGShopBundle:Cart:cart.html.twig', array(
            'cart' => $cart,
            'cartProducts' => $cartProducts
        ));

        $array = array(
            'success' => $success,
            'message' => $message,
            'data_cart' => $data,
            'action' => 'collaborator_add'
        ); // data to return via JSON

        return new JsonResponse($array);

    }

    public function removeProductFromCartAction($productId){
        $success = false;
        $message = '';

        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $cart = null;

        $product = $em->getRepository(Product::class)->find($productId);
        if (!$user->getCart()){

           $message = 'You don\'have a cart';

        } else {
            $cart = $user->getCart();

            $cartProduct = $em->getRepository(CartProduct::class)->findOneBy(array(
                'cart' => $cart,
                'product' => $product
            ));

            if ($cartProduct){

                $em->remove($cartProduct);
                $em->flush();

                $success = true;

            }else{
                $message = 'Nothing to delete.';
            }
        }

        /*return $this->redirectToRoute('scg_shop_homepage');*/

        $cartProducts_cart = $this->getDoctrine()->getRepository(CartProduct::class)->findBy(array('cart' => $cart));
        $data_cart = $this->renderView('SCGShopBundle:Cart:cart.html.twig', array(
            'cart' => $cart,
            'cartProducts' => $cartProducts_cart
        ));
        $cartProducts_order = $em->getRepository(CartProduct::class)->findBy(array('cart' => $cart ));
        $data_order = $this->renderView('SCGShopBundle:Order:order_content.html.twig', array(
            'cart' => $cart,
            'cartProducts' => $cartProducts_order
        ));
        $array = array(
            'success' => $success,
            'message' => $message,
            'data_cart' => $data_cart,
            'data_order' => $data_order,
            'action' => 'removeFromCart'
        ); // data to return via JSON

        return new JsonResponse($array);

    }
}
