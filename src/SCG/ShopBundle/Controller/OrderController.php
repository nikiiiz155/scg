<?php

namespace SCG\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SCG\ShopBundle\Entity\Cart;
use SCG\ShopBundle\Entity\CartProduct;

class OrderController extends Controller
{
    public function viewAction(){

        return $this->render('SCGShopBundle:Order:order.html.twig', array(

        ));

    }

    public function orderAction(){
        $em = $this->getDoctrine()->getManager(); //c'est l'instance du gestionnaire de doctrine pour faire relation entre entité et base de donnée

        $cart = $this->getUser()->getcart();
        $cartProducts = $em->getRepository(CartProduct::class)->findBy(array(
            'cart' => $cart
        ));//get repositery = methode pour la gestion d'objet en base de donnée

        return $this->render('SCGShopBundle:Order:order_content.html.twig', array(
            'cart' => $cart,
            'cartProducts' => $cartProducts
        ));
    }

}