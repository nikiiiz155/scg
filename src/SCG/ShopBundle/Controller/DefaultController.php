<?php
namespace SCG\ShopBundle\Controller;

use SCG\ShopBundle\Entity\Cart;
use SCG\ShopBundle\Entity\CartProduct;
use SCG\ShopBundle\Entity\Category;
use SCG\ShopBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction(){
        $em = $this->getDoctrine()->getManager(); //c'est l'instance du gestionnaire de doctrine pour faire relation entre entité et base de donnée

        //get repositery = methode pour la gestion d'objet en base de donnée
        $categories = $em->getRepository(Category::class)->findAll();
        $parentProducts = $em->getRepository(Product::class)->findAllParentProducts();
        $subProducts = $em->getRepository(Product::class)->findAllSubProducts();

        return $this->render('SCGShopBundle::catalogue.html.twig', array(
            'categories' => $categories, //parametre qu'on passe à la vue
            'parentProducts' => $parentProducts,
            'subProducts' => $subProducts,
        ));
    }
}
