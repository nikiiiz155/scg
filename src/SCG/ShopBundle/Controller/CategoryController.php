<?php
namespace SCG\ShopBundle\Controller;

use SCG\ShopBundle\Entity\Category;
use SCG\ShopBundle\Form\Type\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller{
    public function newAction(Request $request){
        $category = new Category();
        $form = $this->createForm(CategoryType::class,$category)
            ->add('submit',SubmitType::class,array(
                'label' => 'Add Category'
            ));

        $form->handleRequest($request);
        if ($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('scg_shop_homepage');
        }

        return $this->render('@SCGShop/Category/form.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request,$categoryId){
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(Category::class)->find($categoryId);
        $form = $this->createForm(CategoryType::class,$category)
            ->add('submit',SubmitType::class,array(
                'label' => 'Save Category'
            ));

        $form->handleRequest($request);
        if ($form->isValid()){
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('scg_shop_homepage');
        }

        return $this->render('@SCGShop/Category/form.html.twig',array(
            'form' => $form->createView()
        ));
    }
}
