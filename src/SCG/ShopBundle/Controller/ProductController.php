<?php
namespace SCG\ShopBundle\Controller;

use SCG\ShopBundle\Entity\Product;
use SCG\ShopBundle\Form\Type\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller{
    public function newAction(Request $request){
        $product = new Product();
        $form = $this->createForm(ProductType::class,$product)
            ->add('submit',SubmitType::class,array(
                'label' => 'Add Product'
            ));

        $form->handleRequest($request);
        if ($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('scg_shop_homepage');
        }

        return $this->render('@SCGShop/Category/form.html.twig',array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request,$productId){
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($productId);
        $form = $this->createForm(ProductType::class,$product)
            ->add('submit',SubmitType::class,array(
                'label' => 'Save Product'
            ));

        $form->handleRequest($request);
        if ($form->isValid()){
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('scg_shop_homepage');
        }

        return $this->render('@SCGShop/Category/form.html.twig',array(
            'form' => $form->createView()
        ));
    }
}
