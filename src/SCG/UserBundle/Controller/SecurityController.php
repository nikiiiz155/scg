<?php

namespace SCG\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

// use Symfony\Component\Security\Core\SecurityContext; => DEPRECATED

class SecurityController extends BaseController
{
    public function loginModalAction () {
        $request = $this->get('request_stack')->getCurrentRequest();
        // $request = $this->container->get('request'); => DEPRECATED, see http://symfony.com/blog/new-in-symfony-2-4-the-request-stack
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $session = $request->getSession();
        /* @var $session \Symfony\Component\HttpFoundation\Session\Session */
        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }
        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(Security::LAST_USERNAME);
        $csrfToken = $this->has('security.csrf.token_manager') ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue() : null;
        return $this->renderLoginModal(array(
            'last_username' => $lastUsername,  //Paramètres envoyés vers le template du login
            'error'         => $error,
            'csrf_token'    => $csrfToken
        ));
    }
    protected function renderLoginModal(array $data)
    {
        $template = sprintf('SCGUserBundle:Security:login_content.html.twig'); //Template qui sera rendu afin d'être affiché
        // $template = sprintf('FOSUserBundle:Security:login_content.html.%s', $this->container->getParameter('fos_user.template.engine')); => DEPRECATED
        return $this->get('templating')->renderResponse($template, $data);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request){
        if($this->isGranted('IS_AUTHENTICATED_FULLY') ||  $this->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $this->addFlash('flash_warning',"You are already logged in");
            return $this->redirectToRoute('homepage');
        }

        return parent::loginAction($request);
    }
}