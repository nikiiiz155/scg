<?php

namespace SCG\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SCGUserBundle:Default:index.html.twig');
    }
}
