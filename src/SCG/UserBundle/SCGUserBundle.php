<?php

namespace SCG\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SCGUserBundle extends Bundle
{
    public function getParent(){
        return 'FOSUserBundle';
    }
}