<?php

namespace SCG\UserBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('street',TextType::class, array('label'=>'Rue'))
        ->add('zip',IntegerType::class, array('label'=>'Code postal'))
        ->add('city',TextType::class, array('label'=>'Ville'))
        ->add('country',TextType::class, array('label'=>'Pays'))
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'scg_user_registration';
    }

    public function getStreet()
    {
        return $this->getBlockPrefix();
    }

    public function getZip()
    {
        return $this->getBlockPrefix();
    }

    public function getCity()
    {
        return $this->getBlockPrefix();
    }

    public function getCountry()
    {
        return $this->getBlockPrefix();
    }
}