<?php
namespace SCG\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    public function localeAction(Request $request){
        $locale = $request->getLocale();
        return $this->redirectToRoute('homepage',array(
            '_locale' => $locale
        ));
    }

    public function indexAction(){
        return $this->render(':default:index.html.twig');
    }

    public function abonnementAction(){
        return $this->render(':default:abonnement.html.twig');
    }

    public function localisationAction(){
        return $this->render(':default:localisation.html.twig');
    }
}